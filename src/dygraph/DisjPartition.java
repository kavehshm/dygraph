package dygraph;
import java.util.*;
import dygraph.Graph.Edge;

public class DisjPartition {
	
	private Graph gp;
	private DfsTree T;
	
	protected Updates U;
	
	public BitSet IsRootuLst;
	public BitSet TPbarInLst;
	public BitSet PathEnd;
	
	public Map<Integer, Integer> pathMapL;
	public Map<Integer, Integer> pathMapR;
	public BiMap<Integer> pathMap;
	public Map<Integer, Integer> treeRangeMap;	
	
		
	public DisjPartition(Graph gp,
			DfsTree dfstree,
			Dstruct Dstr) {
		
		this.gp = gp;
		this.T = dfstree;
		IsRootuLst = new BitSet();
		TPbarInLst = new BitSet();

		pathMap = new BiMap<Integer>();
		treeRangeMap = new HashMap<Integer, Integer>();
		
		firstPartition();
	}
	
	public boolean IsRoot(Integer nd) {
		return IsRootuLst.get(toLstPos(nd));
	}
	public boolean TPbar(Integer nd) {
		return TPbarInLst.get(toLstPos(nd));
	}
	
	public void setTPbar(Integer nd, boolean state) {
		TPbarInLst.set(toLstPos(nd), state);
	}
	public void setIsRoot(Integer nd, boolean state) {
		IsRootuLst.set(toLstPos(nd), state);
	}
	public void addPath(Integer u, Integer v) {
		pathMap.addPair(u, v);
	}
	public Integer getPathOtherEnd(Integer u) {
		return pathMap.get(u);
	}
	public boolean IsPathEnd(Integer u) {
		return pathMap.contains(u);
	}
	public ArrayList<Integer> getPathNodes(Integer uanc, Integer vdep) {
		ArrayList<Integer> retlist = new ArrayList<Integer>();
		Integer par = T.getPar(vdep);
		do {
			retlist.add(par);
			par = T.getPar(vdep);
		}
		while (par != uanc);
		return retlist;
	}
	public Set<Map.Entry<Integer, Integer>> getPathSet() { 
		return pathMap.getMapL().entrySet(); 
	}
	public Set<Map.Entry<Integer, Integer>> getTreeSet() {
		return treeRangeMap.entrySet();
	}
	public Set<Map.Entry<Integer, Integer>> getPathNodeSet() {
		Map<Integer, Integer> pathNodeMap = new HashMap<Integer, Integer>();
		for (Map.Entry<Integer, Integer> path : pathMap.getMapL().entrySet()) {
			pathNodeMap.put(path.getKey(), path.getValue());
		}
		return pathNodeMap.entrySet();
	}
	public Set<Map.Entry<Integer, Integer>> getTreeNodeSet() {
		Map<Integer, Integer> treeNodeMap = new HashMap<Integer, Integer>();
		for (Map.Entry<Integer, Integer> tree : treeRangeMap.entrySet()) {
			treeNodeMap.put(Lst(tree.getKey()), Lst(tree.getValue()));
		}
		return treeNodeMap.entrySet();
	}
	public void changePathEnd(Integer u, Integer unew) {
		pathMap.changeKey(u, unew);
	}
	public void removePath(Integer u, Integer v)  {
		pathMap.removePair(u, v);
	}
	
	public Integer findPathHead(Integer vf) {
		Integer par = vf;
		do{
			par = T.getPar(par);
		}
		while (getPathOtherEnd(par) == null);
		return par;
	}
	
	public Pair<Integer> PathParam(Integer nd) {
		Pair<Integer> uv = new Pair<Integer>();
		uv.L = findPathHead(nd);
		uv.R = getPathOtherEnd(uv.L);
		
		return uv;
	}
	
	public Integer getTreeLast(Integer w) {
		return treeRangeMap.get(w);
	}
	
	
	public void firstPartition() {
	
		TPbarInLst.set(0, T.Lst.size()+1, true);
		IsRootuLst.set(0, T.Lst.size()+1, false);
		
		for (Integer nd : T.getchildren(0) ) {
			setIsRoot(nd, true);
		}
	}
	
	public Integer toLstPos(Integer nd) {
		// @Testing
		if ( T.LstMap.get(nd) == null) {
			System.out.println("node not found in LstMap");
			System.exit(1);
		}
		return T.LstMap.get(nd);
	}
	
	public Integer Lst(Integer pos) {
		return T.Lst(pos);
	}
	
	// tree partitioning routines
	public void partitionTreeAt(Integer vf) {
		setTPbar(vf, false);
		for (Integer nd : T.getchildren(vf) ) {
			setIsRoot(nd, true);
		}
		
		if (IsRoot(vf)) {
			return;
		}
		
		Integer curnd = T.getPar(vf);
		Integer pathStart = new Integer(curnd);
		Integer child = vf;
		
		while (true) {
			setTPbar(curnd, false);
			List<Integer> otherChildren = T.getOtherChildren(curnd, child);
			if (otherChildren.size() > 0) {
				for (Integer c : otherChildren)
					setIsRoot(c, true);
			}
			if (IsRoot(curnd))
				break;
			child = curnd;
			curnd = T.getPar(curnd);
			// @Testing
			if (curnd == -1) {
				System.err.println("problem");
				System.exit(1);
			}
		}
		setTPbar(curnd, false);
		setIsRoot(curnd, false);
		addPath(curnd, pathStart);
	}

	public Integer findChildinEdge(Edge e) {
		Integer child = 0;
		if ( T.getPar(e.L).equals(e.R) ) {
			child = new Integer(e.L);
		}
		else {
			child = new Integer(e.R);
		}
		return child;
	}
	
	public void partitionTreeAt(Edge e) {
		
		Integer vf = 0, par = 0;
		
		vf = findChildinEdge(e);
		par = T.getPar(vf);
		
		setIsRoot(vf, true);
	
		Integer pathStart = new Integer(par);
		Integer child = vf;
		
		while (true) {
			setTPbar(par, false);
			List<Integer> otherChildren = T.getOtherChildren(par, child);
			if (otherChildren.size() > 0) {
				for (Integer c : otherChildren)
					setIsRoot(c, true);
			}
			if (IsRoot(par))
				break;
			child = par;
			par = T.getPar(par);
			// @Testing
			if (par == -1) {
				System.err.println("problem");
				System.exit(1);
			}
		}
		setTPbar(par, false);
		setIsRoot(par, false);
		addPath(par, pathStart);
	}
	
	public ArrayList<Integer> goToTreeRoot(Integer u) {
		if (TPbar(u) != true) {
			System.err.println("not a tree");
		}
		
		ArrayList<Integer> uvpath = new ArrayList<Integer>();
		Integer par = u;
		do {
			uvpath.add(par);
			par = T.getPar(par);
		}
		while (!IsRoot(par));
		return uvpath;
	}

	public void splitPathAtNode(Integer split) {
		Integer x = getPathOtherEnd(split);
		if (x != null) {
			changePathEnd(split, T.getPar(split));
		}
		else {
			Pair<Integer> uv = PathParam(split);
			splitPath(uv.L, split, T.getPar(split), uv.R);
		}
	}
	
	public void splitPathAtEdge(Edge e) {
		Integer childInE = 0, parInE = 0;
		childInE = findChildinEdge(e);
		parInE = T.getPar(childInE);
		
		Pair<Integer> uv = PathParam(parInE);
		splitPath(uv.L, parInE, childInE, uv.R);
	}
	
	public boolean edgeOnBoundry(Edge e) {
		if ((TPbar(e.L) != TPbar(e.R)))
			return true;
		
		if ( IsPathEnd(e.L) || IsPathEnd(e.R) )
			return true;
		
		return false;
		
	}
	
	// u->v path is split to u->x, y->v
	public void splitPath(Integer u, Integer x, Integer y, Integer v) {
		removePath(u, v);
		addPath(u, x);
		addPath(y, v);
	}
	
	// main method for applying updates
	public void applyUpdates(Updates up) {
		
		this.U = up;
		
		for (Integer vf : up.nodeDels) {
			if (TPbar(vf)) {
				//System.out.println(vf + " is in trees");
				partitionTreeAt(vf);
			}
			if (!TPbar(vf)) {
				//System.out.println(vf + " is in paths");
				splitPathAtNode(vf);
			}
		}
		
		for (Edge e : up.edgeDels) {
			if (!edgeOnBoundry(e)) {
				if (TPbar(e.L)) {
					partitionTreeAt(e);
				}
				else {
					splitPathAtEdge(e);
				}
			}
		}

		updateTreeRangeMap();
	}
	
	public void updateTreeRangeMap() {
		
		treeRangeMap.clear();
		for (int i = IsRootuLst.nextSetBit(0); i >= 0; i = IsRootuLst.nextSetBit(i+1)) {
			if (IsRootuLst.nextSetBit(i+1) == -1)
				treeRangeMap.put(i, i);
			else {
				int end = IsRootuLst.nextSetBit(i+1)-1;
				while (!TPbar(Lst(end))) {
					end = end-1;
				}
				treeRangeMap.put(i, end);
			}		
		 }
	}
	
	public void colorPartition(stream.Stream strm) {
		for (Integer nd : gp.getNodeList()) {
			if (nd != 0) {
				if (TPbar(nd))
					strm.colorNode(nd, "red"); // trees are red
				else 
					strm.colorNode(nd, "blue"); // and paths are blue
			}
		}
		
		for (int i = IsRootuLst.nextSetBit(0); i >= 0; i = IsRootuLst.nextSetBit(i+1)) {
			if (Lst(i) != null)
				strm.colorNode(Lst(i), "darkred"); // tree roots are dark red
		}
		
		if ( U != null) {
			for (Integer nd : U.nodeDels) {
				strm.colorNode(nd, "green");
			}
			for (Edge e : U.edgeDels) {
				strm.colorEdge(e, "green");
			}
		}
	}
	
	public void plotPartition() {
		stream.Stream dystrm = new stream.Stream(gp);
		dystrm.plotDfsTree(T);
		colorPartition(dystrm);
	}
	
	public static void main(String args[]){
		
		System.out.println("Testing Disjoint-tree-partitioning");
		
		Graph gp = GenGraph.testGraph2();
		gp.print();
		DyDfsTree dydfs = new DyDfsTree(gp);
		gp.print();
		
		System.out.println("Lst: " + dydfs.P.T.Lst);
		
		dydfs.P.plotPartition();
	
		System.out.println("Last loc map " + dydfs.T.LastNodeMap);
		
		dydfs.delNode(20);
		//dydfs.delNode(14);
		dydfs.delEdge(4, 6);
		//dydfs.delEdge(4, 5);
		//dydfs.delEdge(22, 20);
		//dydfs.delEdge(9, 17);
		dydfs.P.applyUpdates(dydfs.U);
		
		dydfs.P.plotPartition();
		
		System.out.println("path set: " + dydfs.P.getPathNodeSet());
		System.out.println("tree set:" + dydfs.P.getTreeNodeSet());
    }



}
