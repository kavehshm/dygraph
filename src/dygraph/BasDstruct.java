package dygraph;
import java.util.*;

import dygraph.BinTree.BSTree;
import dygraph.BinTree.BSTree.Report;
import dygraph.Graph.Edge;

public class BasDstruct extends Dstruct {
	
	protected static boolean verbose = false;
	protected static Integer minEdge;
	protected static Integer maxEdge;
	protected static Integer queryEdgeU;
	protected static Integer queryEdgeV;
	protected BaswTree bswtree;
	
	public DfsTree T;
	
	public class BaswTree extends BSTree < BSTree<Integer> > {
		
		
		public BaswTree (Graph gp) {
			this.root = new Node();
			ArrayList<Integer> conLst = Utility.consecList(0, Lst.size()-1); 
			buildTree(root, conLst, gp);
		}
	
		protected void buildTree(Node node, List<Integer> XList, Graph gp) {
			if (XList.size() == 0)
				return;

			if (XList.size() == 1) {
				node.key = XList.get(0);
				AugmentNode(node, XList, gp);
				return;
			}
			
			else {
				Integer xmed = ((XList.size()-1)/2);
				node.key = XList.get(xmed);
				AugmentNode(node, XList, gp);
				node.left = new Node();
				node.right = new Node();
				buildTree(node.left, XList.subList(0, xmed+1), gp);
				buildTree(node.right, XList.subList(xmed+1, XList.size()), gp);
			}
		}
		protected Comparator<Edge> CompareEdge = new Comparator<Edge> () {
			public int compare(Edge e1, Edge e2) {
				return ( T.Pos(e1.getV()) > T.Pos(e2.getV()) ) ? 1
						: ( T.Pos(e1.getV()) < T.Pos(e2.getV()) ) ? -1
				         : 0;
			}
		};
		protected void AugmentNode(Node node, List<Integer> XList, Graph gp) {
			Set<Edge> edgeSet = new HashSet<Edge>();
			for (Integer i : XList) {
				 edgeSet.addAll(gp.getEdgeList(T.Lst(i)));
			}
			node.data = new BSTree<Integer>();
			ArrayList<Edge> edgelist = new ArrayList<Edge>(edgeSet);
			Collections.sort(edgelist, CompareEdge);
			if (verbose) Edge.printEdgeList(edgelist);
			buildEdgeTree(node.data.root, edgelist);
		}
		
		protected void buildEdgeTree(BSTree<Integer>.Node node,
				List<Edge> edgelist) {
			if (edgelist.size() == 0)
				return;

			if (edgelist.size() == 1) {
				node.key = T.Pos(edgelist.get(0).getV());
				node.data = T.Pos(edgelist.get(0).getU());
				return;
			}
			
			else {
				Integer xmed = ((edgelist.size()-1)/2);
				node.key = T.firstPos(edgelist.get(xmed).getV());
				node.left = new BSTree<Integer>.Node();
				node.right = new BSTree<Integer>.Node();
				buildEdgeTree(node.left, edgelist.subList(0, xmed+1));
				buildEdgeTree(node.right, edgelist.subList(xmed+1, edgelist.size()));
			}
		}
		
		/*
		 * Edge Query methods
		 */
		public Edge TreeQuery (Integer rootPos, Integer lastPos, 
				Integer xNode, int rev) {
			return TreeQuery(rootPos, lastPos, xNode, -1, rev);
		}
		public Edge TreeQuery (Integer rootPos, Integer lastPos,
				Integer xNode, Integer yNode, int rev) {
			minEdge = T.Pos(xNode);
			if (yNode == -1)
				maxEdge = T.firstPos( T.getPar(T.Lst(rootPos)) );
			else
				maxEdge = T.firstPos(yNode);
			queryEdgeU = (rev == 0) ? Integer.MAX_VALUE : Integer.MIN_VALUE;
			queryEdgeV = Integer.MIN_VALUE;
			ReportBasw r = (rev == 0) ? (new BasQuery()) : (new BasRevQuery());
			if (verbose) System.out.println("min path: " + minEdge +
											" max path: " + maxEdge);
			this.queryRange(rootPos, lastPos, r);
			if (queryEdgeU == ((rev == 0)? Integer.MAX_VALUE : Integer.MIN_VALUE))
				return null;
			else {
				System.out.println("The minimum incident edge is : (" + T.Lst(queryEdgeU) + ", " + T.Lst(queryEdgeV) + ")");
				return new Edge(T.Lst(queryEdgeU), T.Lst(queryEdgeV));
			}
		}

		public Edge NodeQuery (Integer nd, Integer minPathNode, Integer maxPathNode) {
			minEdge = minPathNode;
			maxEdge = maxPathNode;
			queryEdgeU = Integer.MAX_VALUE;
			queryEdgeV = Integer.MIN_VALUE;
			Node w = this.findKey(nd);
			if (w == null) {
				System.err.println("Node not found");
				System.exit(1);
				return null;
			}
			else {
				BSTree<Integer>.Node succ = w.data.successor(minEdge, maxEdge);
				if (succ != null) {
					queryEdgeU = succ.key;
					queryEdgeV = succ.data;
					System.out.println("The minimum node query edge is: (" + queryEdgeU + ", " + queryEdgeV + ")");
					return new Edge(queryEdgeU, queryEdgeV);
				}
				else 
					return null;
			}
		}
		
		/*
		 * updating methods
		 */
		public void removeEdge(Integer u, Integer v) {
			Integer uPos = T.Pos(u);
			Integer vPos = T.Pos(v);
			
			if (verbose) System.out.println("removing edge (" + T.Lst(uPos) + "," + T.Lst(vPos) + ")");
			Node curnode = new Node();
			curnode = root;
			
			while (true) {
				
				if (verbose) System.out.println("\nbefore");
				if (verbose) printEdgeTree(curnode.data.root, 0);
				//curnode.data.printTree();
				
				curnode.data.delete(uPos, vPos);
				
				if (verbose) System.out.println("\nafter");
				if (verbose) printEdgeTree(curnode.data.root, 0);
				//curnode.data.printTree();
				
				if (curnode.IsLeaf())
					return;
				if (vPos > curnode.key) 
					curnode = curnode.right;
				else 
					curnode = curnode.left;
			}
			
		}
		
		
		/*
		 * printing Baswana's augmented binary tree
		 */
		public void printBaswTree() {
			System.out.println("Segment Tree:");
			printTree(root, 0);
			
			System.out.println("Augmentation structures:");
			printAssocTree(root);
		}
		public void printAssocTree(Node node) {
			if(node == null)
		         return;
			
			System.out.println("\nNode in SegTree:" + node.key);
			printEdgeTree(node.data.root, 0);
			
			printAssocTree(node.left);
			printAssocTree(node.right);
		}
		
		public void printEdgeTree(BSTree<Integer>.Node node, int level){
		    if(node == null)
		         return;
		    printEdgeTree(node.right, level+1);
		    if(level != 0){
		        for(int i = 0; i < level-1; i++)
		            System.out.print("|\t");
		        if (node.IsLeaf()) 
		        	System.out.println("|-------(" + T.Lst(node.key) + "," 
		        			+ T.Lst(node.data) + ")");
		        else 
		        	System.out.println("|-------" + node.key);
		    }
		    else
		        System.out.println(node.key);
		    printEdgeTree(node.left, level+1);
		}
		
	};
	
	
	/*
	 * searching query
	 */
	protected interface ReportBasw extends Report<BSTree <Integer> > {}

	protected static class BasQuery implements ReportBasw {
		public void reportTree(BinTree.BSTree< BSTree<Integer> >.Node node) {
			//testing
			if (verbose) System.out.println("querying node " + node.key);
			if (node.data == null)
				return;
			BSTree<Integer>.Node succ = node.data.successor(minEdge, maxEdge, 1);
			if ( succ != null) {
				if (succ.key < BasDstruct.queryEdgeU) {
					queryEdgeU = succ.key;
					queryEdgeV = succ.data;
				}
			}
		}
	}
	
	// reversed query when in (x, y) x is further in the list Lst 
	protected static class BasRevQuery implements ReportBasw {
		public void reportTree(BinTree.BSTree< BSTree<Integer> >.Node node) {
			//testing
			if (verbose) System.out.println("querying node " + node.key);
			if (node.data == null)
				return;
			BSTree<Integer>.Node pred = node.data.predecessor(maxEdge, minEdge, 1);
			if ( pred != null) {
				if (pred.key > BasDstruct.queryEdgeU) {
					queryEdgeU = pred.key;
					queryEdgeV = pred.data;
				}
			}
		}
	}
	
	public Edge NodeQuery(Integer w, Integer x, Integer y) {
		if (x < y)
			return bswtree.NodeQuery(w, x, y);
		else 
			return bswtree.NodeQuery(w, y, x);
			
	}
	
	// (x, y) path where x is par(r) T(r) being the tree in the query
	// direction 0 : lowest edge on (x, par(T(y)) )
	// direction 1 : highest edge on (x, par(T(y)) )
	public Edge TreeQuery(Integer rootNode, Integer xNode) {
		if (T.Pos(xNode) < T.Pos(rootNode)-1 )
			return bswtree.TreeQuery(T.firstPos(rootNode), T.lastPos(rootNode), xNode, 0);
		else
			return null;
	}
	public Edge TreeQuery(Integer rootNode, Integer xNode, int direction) {
		if (direction == 0)
			return bswtree.TreeQuery(T.firstPos(rootNode), T.lastPos(rootNode), xNode, 0);
		else
			return bswtree.TreeQuery(T.firstPos(rootNode), T.lastPos(rootNode), xNode, 1);
	}
	
	public Edge TreeQuery(Integer rootNode, Integer xNode, Integer yNode, int direction) {
		if (direction == 0)
			return bswtree.TreeQuery(T.firstPos(rootNode), 
					T.lastPos(rootNode), xNode, yNode, 0);
		else
			return bswtree.TreeQuery(T.firstPos(rootNode), T.lastPos(rootNode),
					xNode, yNode, 1);
	}
	
	public void removeEdge(Integer uNode, Integer vNode) {
		bswtree.removeEdge(uNode, vNode);
		bswtree.removeEdge(vNode, uNode);
	}
	
	public BasDstruct (Graph gp, DfsTree dfstree) {
		super(gp, dfstree);
		this.T = dfstree;
		this.bswtree = new BaswTree(gp);
	}
	
	public static void main (String args[]) {
		Graph gp = GenGraph.testGraph7();
		DfsTree T = gp.staticDFS();
		DfsTree.plotdfstree(gp, T);
		BasDstruct basD = new BasDstruct(gp, T);
		System.out.println("\nprinting Baswana D struct:\n" );
		basD.bswtree.printBaswTree();
		//basD.TreeQuery(6, 1, 1);
		
		System.out.println("\n");
		basD.removeEdge(6, 9);
		System.out.println("\n");
		//basD.bswtree.printBaswTree();
		
		System.out.println("Performing tree query");
		basD.TreeQuery(3, 1);
		//basD.bswtree.printBaswTree();
		//basD.bswtree.NodeQuery (0, 2, 10);
	}

}


