package stream;

import org.graphstream.algorithm.generator.DorogovtsevMendesGenerator;
import org.graphstream.algorithm.generator.Generator;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class Stream {
	
	
	private static boolean verbose = false;
	
	private dygraph.Graph gp;
	private Graph graph;
	private Graph dfsgraph;
	
	public Stream(dygraph.Graph gp) {
		this.sheet = true;
		this.gp = gp;
		this.graph = new SingleGraph("streamGraph");
	}
	
	public void plotGraph() {
        graph.setStrict(false);
        graph.setAutoCreate(true);
        graph.addAttribute("ui.stylesheet", getStylesheet());
		
        for (dygraph.Graph.Edge e : gp.getAllEdgesSetwR()) {
        	graph.addEdge(e.L.toString() + e.R.toString(),
        			e.L.toString(), e.R.toString());
        }
        
        for (Node node : graph) {
            node.addAttribute("ui.label", node.getId());
            node.setAttribute("ui.class", "marked");
        }
        
        attachNodeID(graph);

        graph.removeNode("0");
        
        graph.display();
        
	}
	
	public static void attachNodeID(Graph ingp) {
        for (Node node : ingp) {
            node.addAttribute("ui.label", node.getId());
            node.setAttribute("ui.class", "marked");
        }
	}
	
	public void colorTree(dygraph.DfsTree tree) {
		for (dygraph.Graph.Edge e : tree.getDfsEdgeList()) {
			//streamWait(10);
			Edge estream = graph.getEdge(e.L.toString() + e.R.toString());
			if (estream == null){
				estream = graph.getEdge(e.R.toString() + e.L.toString());	
			}
			if (estream != null){
				estream.addAttribute("ui.class", "marked");
				estream.setAttribute("ui.class", "marked");
				estream.getNode0().setAttribute("ui.class", "blue");
				estream.getNode1().setAttribute("ui.class", "blue");
			}
		}
	}
	
	public void orderedColorTree(dygraph.DfsTree tree) {
		for (dygraph.Graph.Edge e : tree.getOrderedDfsEdgeList()) {
			streamWait(100);
			Edge estream = graph.getEdge(e.L.toString() + e.R.toString());
			if (estream == null){
				estream = graph.getEdge(e.R.toString() + e.L.toString());	
			}
			if (estream != null){
				estream.addAttribute("ui.class", "marked");
				estream.setAttribute("ui.class", "marked");
				estream.getNode0().setAttribute("ui.class", "blue");
				estream.getNode1().setAttribute("ui.class", "blue");
			}
		}
	}
	
	public void colorUpdates(dygraph.Updates U) {
		for (dygraph.Graph.Edge e : U.edgeDels) {
			Edge estream = findEdge(e.getU(), e.getV());
			if (estream != null) {
				estream.addAttribute("ui.class", "green");
				estream.setAttribute("ui.class", "green");
			}
		}
	}
	
	public Edge findEdge(Integer u, Integer v) {
		Edge estream = graph.getEdge(u.toString() + v.toString());
		if (estream == null){
			estream = graph.getEdge(v.toString() + u.toString());	
		}
		return estream;
		
	}
	
	public void plotDfsTree(dygraph.DfsTree dfstree) {
		this.dfsgraph = new SingleGraph("streamGraph"); 
		
		dfsgraph.setStrict(false);
        dfsgraph.setAutoCreate(true);
        dfsgraph.addAttribute("ui.stylesheet", getStylesheet());
		
		for (dygraph.Graph.Edge e : dfstree.getDfsEdges()) {
			dfsgraph.addEdge(e.L.toString() + e.R.toString(), 
					e.L.toString(), e.R.toString());
		}
		
        dfsgraph.display();
        attachNodeID(dfsgraph);
        
	}
	
	public void colorNode(Integer nd, String color) {
		Node graphnd = dfsgraph.getNode(nd.toString());
		if (color == "red") {
			graphnd.setAttribute("ui.class", "red");
		}
		else if (color == "blue") {
			graphnd.setAttribute("ui.class", "blue");
		}
		else if (color == "darkred") {
			graphnd.setAttribute("ui.class", "darkred");
		}
		else if (color == "green") {
			graphnd.setAttribute("ui.class", "green");
		}
	}

	public void colorEdge(dygraph.Graph.Edge e, String color) {
		
		Edge estream = dfsgraph.getEdge(e.L.toString() + e.R.toString());
		if (estream == null){
			estream = dfsgraph.getEdge(e.R.toString() + e.L.toString());	
		}
		if (estream != null){
			estream.addAttribute("ui.class", "green");
			estream.setAttribute("ui.class", "green");
		}
	}
	
	public void deleteNode(Integer nd) {
		dfsgraph.removeNode(nd.toString());
	}
	
	public static void streamWait(int ticks) {
		try {
		    Thread.sleep(ticks);
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
	}

	
    protected static String styleSheet1 =
    		"node {" +
    		"   text-size: 20px;" +
    		"	size: 30px;" +
		    "	fill-color: gray;" +
		    "	z-index: 2;" +
    		"}" +

    		"node.red {" +
    		"   text-size: 20px;" +
		    "	size: 30px;" +
		    "	fill-color: red;" +
		    "	z-index: 2;" +
    		"}" +
    		
    		"node.darkred {" +
    		"   text-size: 20px;" +
		    "	size: 30px;" +
		    "	fill-color: #a45;" +
		    "	z-index: 2;" +
    		"}" +
		    
    		
			"node.blue {" +
			"   text-size: 20px;" +
			"	size: 30px;" +
			"	fill-color: blue;" +
			"	z-index: 2;" +
			"}" +
			
			"node.green {" +
			"   text-size: 20px;" +
			"	size: 30px;" +
			"	fill-color: green;" +
			"	z-index: 2;" +
			"}" +

			"edge {" +
			"	shape: line;" +
			"	fill-color: #aaa;" +
			"	arrow-size: 3px, 2px;" +
			"	size: 2px;" +
			"}" +
    		
		    "edge.marked {" +
		    "	shape: line;" +
		    "	fill-color: #44f;" +
		    "	arrow-size: 10px, 2px;" +
		    "	size: 3px;" +
		    "}" +
    
		    "edge.green {" +
		    "	shape: line;" +
		    "	fill-color: green;" +
		    "	arrow-size: 10px, 2px;" +
		    "	size: 3px;" +
		    "}";
	
	
	
    protected static String styleSheet2 =
    		"node {" +
    		//"   text-size: 20px;" +
    		//"	size: 30px;" +
		    "	fill-color: gray;" +
		    "	z-index: 2;" +
    		"}" +

    		"node.red {" +
    		//"   text-size: 20px;" +
		    //"	size: 30px;" +
		    "	fill-color: red;" +
		    "	z-index: 2;" +
    		"}" +
    		
    		"node.darkred {" +
    		//"   text-size: 20px;" +
		    //"	size: 30px;" +
		    //"	fill-color: #a45;" +
		    "	z-index: 2;" +
    		"}" +
		    
    		
			"node.blue {" +
			//"   text-size: 20px;" +
			//"	size: 30px;" +
			"	fill-color: blue;" +
			"	z-index: 2;" +
			"}" +
			
			"node.green {" +
			"   text-size: 20px;" +
			"	size: 30px;" +
			"	fill-color: green;" +
			"	z-index: 2;" +
			"}" +

			"edge {" +
			"	shape: line;" +
			"	fill-color: #aaa;" +
			"	arrow-size: 3px, 2px;" +
			//"	size: 2px;" +
			"}" +
    		
		    "edge.marked {" +
		    "	shape: line;" +
		    "	fill-color: #44f;" +
		    "	arrow-size: 10px, 2px;" +
		    "	size: 2px;" +
		    "}" +
    
		    "edge.green {" +
		    "	shape: line;" +
		    "	fill-color: green;" +
		    "	arrow-size: 10px, 2px;" +
		    //"	size: 3px;" +
		    "}";
    
    protected boolean sheet;
    public void setsheet(boolean sheet) {
    	this.sheet = sheet;
    }
    
    
    public String getStylesheet() {
    	return (sheet) ? styleSheet1: styleSheet2;
    }

    public static Graph genDorogov(int numNodes) {
    	
    	Graph gp = new SingleGraph("Dorogovstsev mendes");
		Generator gen = new DorogovtsevMendesGenerator();
		
		gen.addSink(gp);
		gen.begin();
		for(int i=0; i< numNodes; i++) {
			gen.nextEvents();
		}
		gen.end();
		
		//gp.display();
		//attachNodeID(gp);
		
		return gp;
    }
    
	public static void main (String args[]) {
		Graph graph = new SingleGraph("Dorogovtsev mendes");
		Generator gen = new DorogovtsevMendesGenerator();
		gen.addSink(graph);
		gen.begin();
		for(int i=0; i<1; i++) {
			gen.nextEvents();
		}
		gen.end();
		
		graph.display();
		
		if (verbose) for (Edge e : graph.getEachEdge()) {
			System.out.println(e.getNode0().getIndex() + "," + e.getNode1().getIndex());
		}
		attachNodeID(graph);
	}
	
    
}
