package dygraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dygraph.BinTree.BSTree;

public class RangeTree extends BSTree <BSTree <Byte> > {
	
	static boolean verbose = false;
	
	public static void main(String args[]) {
		System.out.println("the best range trees in the world");
		
		//ArrayList<Integer> testlist = (ArrayList<Integer>) Utility.randomList(5, 30, 10);
		//ArrayList<Integer> testlist = 
		//		new ArrayList<Integer>( Arrays.asList(16,17,22,26,28,30,41,42) );
		//Collections.sort(testlist);
		
		//System.out.println(testlist);
		
		ArrayList<Point> plist = new ArrayList<Point> ();
		
		plist.add(new Point(3,6));
		plist.add(new Point(3,8));
		plist.add(new Point(5,6));
		plist.add(new Point(7,2));
		plist.add(new Point(9,3));
		plist.add(new Point(11,5));
		plist.add(new Point(13,8));
		plist.add(new Point(15,15));
		
		RangeTree myxytree = new RangeTree(plist);
		
		//myxytree.printxyTree();
		myxytree.rangeSuccQuery(1, 8, 3, 9);
		
		//myrtree.queryRange(15, 19);
		//myrtree.insert(15);
		//if (!myrtree.delete(30))
		//	System.out.println("\nnode to be deleted not found\n");
		//myrtree.printTree();
		
		
	}
	
	protected static Integer y1;
	protected static Integer y2;
	protected static Integer miny;
	protected static Integer minx;
	
	public RangeTree (ArrayList<Point> PointList) {
		
		root = new Node();
		
		
		ArrayList<Point> byxList = Point.sortByX(PointList);
		if (verbose) Point.printPointList(byxList);
		ArrayList<Integer> xList = Point.getXList(byxList);
		if (verbose) System.out.println(xList); 
		
		buildRangeTree(root, xList, byxList);
	}
	
	protected void buildRangeTree(Node node, List<Integer> xList, List<Point> byxList) {
		
		if (xList.size() == 0)
			return;

		ArrayList<Integer> yList = Point.getYList(byxList);
		Collections.sort(yList);
		if (verbose) System.out.print("byx list:");
		if (verbose) Point.printPointList(byxList);
		if (verbose) System.out.println("x list  :" + xList);
		if (verbose) System.out.println("y list  :" + yList);
		if (verbose) System.out.println();
		
		if (xList.size() == 1) {
			node.key = xList.get(0);
			node.data = new BSTree <Byte> (yList);
			return;
		}
		
		else {
			Integer xListmed = ((xList.size()-1)/2);
			node.key = xList.get(xListmed);
			Integer byxListmed = Point.findXposition(node.key, byxList);
			node.data = new BSTree <Byte> (yList);
			node.left = new Node();
			node.right = new Node();
			buildRangeTree(node.left, 
					xList.subList(0, xListmed+1), 
					byxList.subList(0, byxListmed+1));
			buildRangeTree(node.right, 
					xList.subList(xListmed+1, xList.size()), 
					byxList.subList(byxListmed+1, byxList.size()));
		}
	}
	
	
	/*
	 * 2D query methods
	 */
	public void range2DQuery (Integer x1, Integer x2, Integer y1, Integer y2) {
		RangeTree.y1 = y1;
		RangeTree.y2 = y2;
		if (verbose) System.out.println("Range query:");
		Report2D r = new Range2DReport();
		this.queryRange(x1, x2, r);
	}
	
	public void rangeSuccQuery (Integer x1, Integer x2, Integer y1, Integer y2) {
		RangeTree.y1 = y1;
		RangeTree.y2 = y2;
		RangeTree.miny = Integer.MAX_VALUE;
		RangeTree.minx = Integer.MIN_VALUE;
		Report2D r = new RangeSuccReport();
		this.queryRange(x1, x2, r);
		System.out.println(
				"minimum point in [" + x1 + ", " + x2 + "]x[" + y1 + ", " + y2 + "] : ");
		System.out.println("(" + minx + ", " + miny + ")");
	}
	
	/*
	 * printing and reporting methods
	 */
	public interface Report2D extends Report<BSTree <Byte> > {}
	
	public static class Range2DReport implements Report2D {
		public void reportTree(Node node) {
			if (verbose) System.out.println("associated data query: " + node.key);
			if (verbose) node.data.printTree();
			node.data.query1Drange(y1, y2);
		}
	}

	public static class RangeSuccReport implements Report2D {
		public void reportTree(Node node) {
			if (verbose) System.out.println("associated data query: " + node.key);
			if (verbose) node.data.printTree();
			BSTree<Byte>.Node succ = node.data.successor(y1, 1);
			if ( succ != null) {
				if (succ.key <= RangeTree.miny) {
					RangeTree.miny = succ.key;
					RangeTree.minx = node.key;
				}
			}
		}
	}
	
	
	/*
	 * printing xyRangeTree
	 */
	public void printxyTree() {
		System.out.println("X-dim Tree:");
		printTree(root, 0);
		
		System.out.println("Y-assoc structures:");
		printAssocTree(root);
	}
	public void printAssocTree(Node node) {
		if(node == null)
	         return;
		
		System.out.println("x:" + node.key);
		node.data.printTree();
		
		printAssocTree(node.left);
		printAssocTree(node.right);
	}
	

	
};
