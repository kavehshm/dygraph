package dygraph;

import java.util.ArrayList;
import java.util.List;
import java.util.BitSet;
import java.util.Map;
import java.util.Random;
import java.util.Stack;
import java.util.HashMap;

import dygraph.Graph.Edge;
import dygraph.Utility;

public class DyDfsTree {

	private static boolean verbose = false;
	
	protected Graph G;
	protected DfsTree T;
	protected DfsTree Tstar;
	protected BasDstruct D;
	protected Updates U;
	protected DisjPartition P;

	protected Map<Integer, ArrayList<Integer>> redadj;
	protected BitSet visited;
	protected final int r;

	public DyDfsTree(Graph G) {
		this.T = G.staticDFS();
		this.G = G;
		this.Tstar = new DfsTree();
		
		long startTime = System.nanoTime();
		this.D = new BasDstruct(G, T);
		System.out.println("\nD struct construction took: " 
				+ (float) (System.nanoTime() - startTime)/1000 );
		
		this.U = new Updates();
		this.r = G.getRnode();
		P = new DisjPartition(G, T, D);
		initializeNLists();
		visited = new BitSet();
	}

	/*
	 * helper methods
	 */
	public void delNode(Integer nd) {
		U.delNode(nd);
	}

	public void delEdge(Integer u, Integer v) {
		U.delEdge(u, v);
	}
	public void delRandEdge() {
		Random rand = new Random();
		Integer numnodes = G.getnumNodes();
		Integer index = rand.nextInt(numnodes-3) + 1;
		Edge e = T.getDfsEdgeList().get(index);
		System.out.println("deleting edge " + e);
		U.delEdge(e.getU(), e.getV());
	}

	public ArrayList<Integer> getN(Integer nd) {
		return G.getadj().get(nd);
	}

	public List<Integer> getL(Integer nd) {
		return this.redadj.get(nd);
	}

	public void setL(Integer nd, List<Integer> lst) {
		ArrayList<Integer> Llst = new ArrayList<Integer>(lst);
		redadj.put(nd, Llst);
	}

	public void addtoL(Integer nd, Integer Lnd) {
		redadj.get(nd).add(Lnd);
	}

	public void addtoL(Integer nd, List<Integer> lst) {
		redadj.get(nd).addAll(lst);
	}

	/*
	 * assumes consecutive node IDs
	 */
	public void initializeNLists() {
		redadj = new HashMap<Integer, ArrayList<Integer>>();
		for (int nd = 0; nd < G.getnumNodes(); nd++) {
			ArrayList<Integer> Llst = new ArrayList<Integer>();
			redadj.put(nd, Llst);
		}
	}

	/*
	 * top methods
	 */
	public void ReportDfsTree() {
		P.applyUpdates(U); // P <- partition(T, U)

		Stack<Integer> dydfsstack = new Stack<Integer>();

		dydfsstack.push(r); // stack.push(r)
		visited.set(r); // status(r) <- visited
		setL(r, getN(r)); // L(r) <- N(r)

		while (!dydfsstack.isEmpty()) {

			Integer w = dydfsstack.peek(); // w <= top of stack
			if (getL(w).isEmpty()) {
				dydfsstack.pop();
			} else {
				Integer u = Utility.lastItem(getL(w)); // last item in L(w)
				getL(w).remove(u); // remove u from L(w)
				if (!visited.get(u)) {
					ArrayList<Integer> pathuv;
					if (P.TPbar(u)) {
						pathuv = DFSinTree(u);
					} else {
						pathuv = DFSinPath(u);
					}

					for (Integer x : pathuv) {
						visited.set(x);
					}
					addtoTstar(pathuv);
					Utility.pushAlltoStack(dydfsstack, pathuv);
				}
			}
		}
	}


	public void addtoTstar(ArrayList<Integer> pathuv) {
		for (int i = 0; i < pathuv.size(); i++) {
			// @TESTME: 
			Tstar.addEdge(pathuv.get(i), pathuv.get(i + 1));
		}
	}

	// @TESTME:
	public ArrayList<Integer> DFSinTree(Integer u) {
		ArrayList<Integer> uvpath = P.goToTreeRoot(u);
		P.setIsRoot(uvpath.get(uvpath.size() - 1), false);

		for (Integer w : uvpath) {
			for (Map.Entry<Integer, Integer> path : P.getPathSet()) {
				Edge e = D.NodeQuery(w, path.getKey(), path.getValue());
				if (e != null) {
					getL(w).add(e.R);
				}
			}

			if (T.getOtherChild(w, T.getPar(w)) != null) {
				Edge yz = D.TreeQuery(w, P.getTreeLast(w), uvpath.get(0));
				getL(yz.L).add(yz.R);
				P.partitionTreeAt(w);
			}
		}
		return uvpath;
	}

	public ArrayList<Integer> DFSinPath(Integer u) {
		Integer v = P.getPathOtherEnd(u);
		P.splitPathAtEdge(new Edge(u, v));
		ArrayList<Integer> uvpath = P.getPathNodes(v, u);
		for (Integer w : uvpath) {
			Edge e = D.NodeQuery(w, Utility.firstItem(uvpath),
					Utility.lastItem(uvpath));
			if (e != null) {
				getL(w).add(e.R);
			}
		}

		for (Map.Entry<Integer, Integer> tree : P.getTreeSet()) {
			Edge e = D.TreeQuery(tree.getKey(), tree.getValue(), u);
			getL(e.L).add(e.R);
		}

		return uvpath;
	}
	
	/*
	 * single update routines
	 */
	
	public void singleUpdateDFS() {
		Edge e = U.edgeDels.get(0);
		if (T.getPar(e) == null) 
			return;
		else {
			Integer maxEdge = T.getPar(e);
			T.removeEdge(e);
			D.removeEdge(e.getU(), e.getV());
			if (verbose) D.bswtree.printBaswTree();
			//T.print();
			System.out.println("reconnect query is " + e.getV() + " at pos " +
					T.firstPos(e.getV()) + " to " + T.lastPos(e.getV()) 
					+ " and 0 to " + maxEdge);
			Edge q = D.TreeQuery(e.getV(), new Integer(0), maxEdge, 0);
			if (q != null) {
				reRoot(e.getV(), q.getV());
				T.addEdge(q.getV(), q.getU());
			}
		}
	}
	
	public void reRoot(Integer prevRootNode, Integer newRootNode) {
		
		Integer curnode = newRootNode;
		Integer child;
		int i = 0;
		while (!curnode.equals(prevRootNode) && i < 30) {
			for (Integer sib : T.getSibligns(curnode)) {
				Edge e = D.TreeQuery(sib, prevRootNode);
				if (e != null) {
					reRoot(sib, e.getV());
					T.addEdge(e.getV(), e.getU());
				}
			}
			child = curnode;
			curnode = T.getPar(curnode);
			System.out.println(curnode + " " + child + " " + prevRootNode);
			T.removeEdge(curnode, child);
			T.addEdge(child, curnode);
			i++;

		}
		
	}
	
	public static boolean InTree(Edge e, DfsTree dtree) {
		Integer parU = dtree.getPar(e.getU());
		Integer parV = dtree.getPar(e.getV());
		
		return ((parU == e.getV()) || (parV == e.getU()));
	}

	/*
	 * main function for testing
	 */
	public static void main(String args[]) {

		//Graph gp = GenGraph.g(true, 10);
		Graph gp = GenGraph.generateDorogov(10);
		//Graph gp = GenGraph.testGraph3();
		System.out.println("Fault Tolerant DFS tree");
		//gp.print();
		DyDfsTree dydfs = new DyDfsTree(gp);
		
		DfsTree.plotdfstree(gp, dydfs.T, dydfs.U);
		
		dydfs.delRandEdge();
		//dydfs.delEdge(9, 6);
		
		long startTime = System.nanoTime();
		dydfs.singleUpdateDFS();
		long estimateTime = System.nanoTime() - startTime;
		
		System.out.println("\ndynamic Dfs time: " + (float) estimateTime/1000);
		
		//dydfs.T.print();
		DfsTree.plotdfstree(gp, dydfs.T, dydfs.U);
	}

}
