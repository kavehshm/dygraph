package dygraph;
import java.util.*;

import dygraph.Graph.Edge;

public class DfsTree {
	
	private static boolean verbose = true;
	
	public Map<Integer, DfsNode> nodemap;
	private Graph gp;
	private BitSet visited;
	private List<Graph.Edge> dfsEdgeList;
	
	/*
	 * variables required for first() and last() queries
	 */
	protected ArrayList<Integer> Lst;
	protected Map<Integer, Integer> LstMap;
	protected Map<Integer, Integer> LastNodeMap;
	
	
	public static class DfsNode {
		
		public List<Integer> children;
		public Integer parent;
		
		public DfsNode() {
			children = new ArrayList<Integer>();
			parent = -1;
		}
		
		public List<Integer> getChildren() {
			return children;
		}
		
		public Integer getPar() { return parent; }
	}
	
	public DfsTree() {
		nodemap = new HashMap<Integer, DfsNode>();
		visited = new BitSet();
		dfsEdgeList = new ArrayList<Graph.Edge>();
	}
	
	
	public DfsTree(Graph gp) {
		
		this.gp = gp;
		addRnode();
		
		nodemap = new HashMap<Integer, DfsNode>();
		visited = new BitSet();
		dfsEdgeList = new ArrayList<Graph.Edge>();
		
		this.Lst = new ArrayList<Integer>();
		this.LstMap = new HashMap<Integer, Integer>();
		this.gp = gp;
		
		LastNodeMap = new HashMap<Integer, Integer>();

		
		
		for (Integer nodeid : this.gp.getadj().keySet()) {
			DfsNode dfsnode = new DfsNode();
			nodemap.put(nodeid, dfsnode);
		}

	}
	
	/*
	 * helper methods
	 */
	public DfsNode getDfsNode(Integer nd) {
		return nodemap.get(nd);
	}
	public Set<Graph.Edge> getDfsEdges() {
		Set<Graph.Edge> retSet = new HashSet<Graph.Edge>();
		for (Map.Entry<Integer, DfsNode> dfsnd : nodemap.entrySet()) {
			for (Integer child : dfsnd.getValue().getChildren()) {
				retSet.add(new Graph.Edge(dfsnd.getKey(), child));
			}
		}
		return retSet;
	}
	public List<Graph.Edge> getDfsEdgeList() {
		dfsEdgeList = new ArrayList<Edge>(getDfsEdges());
		return dfsEdgeList;
	}
	
	public List<Graph.Edge> getOrderedDfsEdgeList() {
		return dfsEdgeList;
	}
 	
	public List<Integer> getChildren(Integer nd) {
		if (getDfsNode(nd) == null)
			System.err.println("Node " + nd + " not present in DFS tree");
		return getDfsNode(nd).getChildren();
	}
	
	public List<Integer> getSibligns(Integer nd) {
		List<Integer> retList = new ArrayList<Integer>();
		if (getPar(nd) == null || getPar(nd) == -1)
			return retList;
		for (Integer c : getChildren(getPar(nd))) {
			if (!c.equals(nd)) {
				retList.add(c);
			}
		}
		return retList;
	}

	
	public void staticDFS() {
	
		Stack<Integer> dfsstack = new Stack<Integer>();
		
		dfsstack.push(0);
		visited.set(0);
		
		boolean found = false;
		while (!dfsstack.isEmpty()) {
			
			Integer w = dfsstack.peek();
			
			for (Integer u : gp.getadj().get(w) ) {
				if (visited.get(u) == false) {
					if (verbose) System.out.println("traversed: " + w + " -> " + u);
					this.addEdge(w, u);
					visited.set(u);
					dfsstack.push(u);
					found = true;
					break;
				}
			}
			if (!found)
				dfsstack.pop();
			else
				found = false;
		}
		
	}
	
	
	private void addRnode() {
		
		gp.addNode(0);
		for (Map.Entry<Integer, ArrayList<Integer>> node : gp.getadj().entrySet()) {
			if (node.getKey() != 0) {
				gp.addEdge(0, node.getKey());
			}
		}
	}
	
	public void addEdge(Integer u, Integer v) {
		if (!nodemap.containsKey(u) || !nodemap.containsKey(v)) {
			System.err.println("node " + u + " or " + 
					v + " not present in DFS tree in addEdge function");
		}

		nodemap.get(v).parent = u;
		nodemap.get(u).children.add(v);
		
		dfsEdgeList.add(new Edge(u, v));
	}
	
	public void removeEdge(Edge e) {
		Integer par = getPar(e);
		Integer child = getChild(e);
		getDfsNode(par).children.remove((Object) child);
		getDfsNode(child).parent = -1;
	}
	
	public void removeEdge(Integer par, Integer child) {
		try {
			getDfsNode(par).children.remove((Object) child);
			getDfsNode(child).parent = -1;
		} catch (NullPointerException e) {
			return;
		}
	}
	
	public Integer getPar(Edge e) {
		if (getPar(e.getU()).equals(e.getV()))
			return e.getV();
		if (getPar(e.getV()).equals(e.getU()))
			return e.getU();
		else {
			System.out.println("edge not in DFS tree");
			return null;
		}
	}
	public Integer getChild(Edge e) {
		if (getPar(e.getU()).equals(e.getV()))
			return e.getU();
		if (getPar(e.getV()).equals(e.getU()))
			return e.getV();
		else {
			System.out.println("edge not in DFS tree");
			return null;
		}
	}

	public void print() {
		System.out.println("parent and child DFS tree:");
		for (Map.Entry<Integer, DfsNode> node : nodemap.entrySet()) {
			System.out.println(node.getKey() +  " par: " 
					+ node.getValue().parent + " children: " 
					+ node.getValue().children.toString() );
		}
	}
	
	public Integer getPar(Integer v) {
		return nodemap.get(v).parent;
	}
	public void setPar(Integer v, Integer par) {
		nodemap.get(v).parent = par;
		nodemap.get(par).children.add(v);
	}
	
	public List<Integer> getchildren(Integer v) {
		return nodemap.get(v).children;
	}
	
	public Integer getChild(Integer v) {
		if (nodemap.get(v).children.size() == 0){
			System.err.println("Node is leaf");
			System.exit(1);
			return null;
		}
		return nodemap.get(v).children.get(0);
	}
	
	public Integer getOtherChild(Integer par, Integer thechild) {
	
		DfsNode parent = nodemap.get(par);
		if (parent.children.size() == 1) 
			return null;
			
		Integer firstChild = nodemap.get(par).children.get(0);
		Integer SecondChild = nodemap.get(par).children.get(1);
		
		if (firstChild == thechild) {
			return SecondChild;
		}
		else {
			return firstChild;
		}
	}
	
	public List<Integer> getOtherChildren(Integer par, Integer badchild) {
		
		List<Integer> retList = new ArrayList<Integer>();
		
		DfsNode parent = nodemap.get(par);
		if (parent.children.size() == 0) 
			return null;
			
		for (Integer child : parent.children) {
			if (!child.equals(badchild)) {
				retList.add(child);
			}
		}
		
		return retList;
	}
	
	/*
	 * Methods for lst, last() and first() queries are now moved to DfsTree
	 */
	
	protected void buildL () {
		Map<Integer, Integer> subtreesize = new HashMap<Integer, Integer> ();
		
		Stack<Integer> stack = new Stack<Integer> ();
		
		stack.push(0);
		
		boolean complete = true;
		
		// compute size map
		while (!stack.isEmpty()) {
			Integer curnode = stack.peek();
	
			if (getchildren(curnode).isEmpty()) {
				subtreesize.put(curnode, 1);
				stack.pop();
				continue;
			}
			
			for (Integer nd : getchildren(curnode)) {
				if (!subtreesize.containsKey(nd)) {
					stack.push(nd);
					complete = false;
				}
			}
			
			if (complete) {
				
				if (getchildren(curnode).size() == 1) {
					subtreesize.put(curnode, 
							1 + subtreesize.get(getchildren(curnode).get(0)));
				}
				else {
					Integer childrenSum  = 0;
					for (Integer chld : getchildren(curnode)) {
						childrenSum += subtreesize.get(chld);
					}
					subtreesize.put(curnode, childrenSum);
				}
				
				stack.pop();
			}
			
			complete = true;
		}
		
		//System.out.println(subtreesize);
		
		//compute L
		
		//ArrayList<Integer> L = new ArrayList<Integer>();
		
		stack.clear();
		
		stack.push(0);
		//this.Lst.add(0);
		Integer curnode = 0;
		
		while (!stack.isEmpty()) {
			
			if (getchildren(curnode).isEmpty()) {
				curnode = stack.pop();
				Lst.add(curnode);
				LstMap.put(curnode, Lst.size()-1);
				continue;
			}
			
			if (getchildren(curnode).size() == 1) {
				Integer nodetoadd = getchildren(curnode).get(0);
				Lst.add(nodetoadd);
				LstMap.put(nodetoadd, Lst.size()-1);
				curnode = getchildren(curnode).get(0);
				continue;
			}
			
			Integer max = subtreesize.get(getchildren(curnode).get(0));
			Integer maxchild = getchildren(curnode).get(0);
			for (Integer chld : getchildren(curnode)) {
				if (subtreesize.get(chld) > max) {
					maxchild = chld;
					max = subtreesize.get(chld);
				}
			}	
			Integer parent = curnode;
			curnode = maxchild;
			Lst.add(curnode);
			LstMap.put(curnode, Lst.size()-1);
			
			for (Integer chld : getchildren(parent)) {
				if (!chld.equals(maxchild)) {
					stack.push(chld);
				}
			}
		}
		
		Lst.remove(Lst.size()-1);
		LstMap.put(0, 0);
		
		if (verbose) { 
			System.out.println("The L list: " + Lst);
			System.out.println("The L Map: " + LstMap);
		}
	}
	
	public void buildLastMap() {
		
		LastNodeMap.put(0, Lst(Lst.size()-1));
		for (Integer nd : Lst) {
			Integer minLstPos = Integer.MAX_VALUE;
			for (Integer sib: getSibligns(nd)){
				Integer sibPos = Pos(sib);
				if ( (sibPos < minLstPos) && (sibPos > Pos(nd)) ) {
					minLstPos = sibPos - 1;
				}
			}
			if (minLstPos.equals(Integer.MAX_VALUE)) {
				minLstPos = LstMap.get(LastNodeMap.get(getPar(nd)));
			}
			if (minLstPos == null) 
				LastNodeMap.put(nd, nd);
			else
				LastNodeMap.put(nd, Lst(minLstPos));
		}
		if (verbose) System.out.println("Last position map: " + LastNodeMap);
	}
	public Integer Lst(Integer pos) {
		return Lst.get(pos);
	}
	
	// from node to position
	public Integer firstPos(Integer nd) {
		return LstMap.get(nd);
	}
	public Integer Pos(Integer nd) {
		if (LstMap.get(nd) == null) {
			System.err.println("Node " + nd + " not in LstMap");
			return null;
		}
		else 
			return LstMap.get(nd);
	}
	// from node to position
	public Integer lastPos(Integer nd) {
		return LstMap.get(LastNodeMap.get(nd));
	}	
	
	public static void plotdfstree(Graph gp, DfsTree T, Updates U) {
		stream.Stream gpstrm = new stream.Stream(gp);
		gpstrm.plotGraph();
		stream.Stream.streamWait(10);
		gpstrm.colorTree(T);
		gpstrm.colorUpdates(U);
		return;
	}
	
	public static void plotdfstree(Graph gp, DfsTree T) {
		stream.Stream gpstrm = new stream.Stream(gp);
		gpstrm.plotGraph();
		stream.Stream.streamWait(200);
		gpstrm.colorTree(T);
		return;
	}
	
	public static void main(String args[]){
		
		System.out.println("Testing DFS tree");
		
		Graph gp = GenGraph.generateDorogov(100);
		DfsTree T = gp.staticDFS();
		stream.Stream gpstrm = new stream.Stream(gp);
		gpstrm.setsheet(false);
		gpstrm.plotGraph();
		gpstrm.orderedColorTree(T);
    }  	

	
}
