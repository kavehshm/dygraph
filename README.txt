In order to run all applications together, run the bash script runnscript.sh using a Linux/Mac/Unix command line.

The script assumes that java is installed at /usr/bin/java 

A better alternative is to import this directory as a project in Eclipse-Java and then run each class using the Run command in eclipse.


