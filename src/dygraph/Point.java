package dygraph;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

public class Point {
	public Integer x;
	public Integer y;
	
	public Integer getX() { return x; }
	public Integer getY() { return y; }
	
	public Point (Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}

	
    protected static ArrayList<Point> sortByX(ArrayList<Point> byxList) {
        Collections.sort(byxList, new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                return Double.compare(p1.getX(), p2.getX());
            }
        });
        return byxList;
    }

    protected static ArrayList<Point> sortByY(ArrayList<Point> byyList) {
        Collections.sort(byyList, new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                return Double.compare(p1.getY(), p2.getY());
            }
        });
        return byyList;
    }
    
    // get the x-coordinates of points in a list without duplicates
    protected static ArrayList<Integer> getXList(List<Point> xyList) {
    	Set<Integer> xSet = new HashSet<Integer>();
        for (Point p : xyList) {
        	xSet.add(p.getX());
        }
        ArrayList<Integer> xList = new ArrayList<Integer>(xSet);
        return xList;
    }
    
    // get the y-cooridinates of points in a list wihtout duplicates
    protected static ArrayList<Integer> getYList(List<Point> xyList) {
    	Set<Integer> ySet = new HashSet<Integer>();
        for (Point p : xyList) {
        	ySet.add(p.getY());
        }
        ArrayList<Integer> yList = new ArrayList<Integer>(ySet);
        return yList;
    }
    
    // split at specific x value
    protected static Integer findXposition(int xval, List<Point> pList) {
    	int iup = (pList.size()-1)/2;
    	int idn = iup;
    	while (true) {
    		if (pList.get(iup).getX() == xval) 
				return iup;
    		if (iup < pList.size() - 1)
    			iup++;
    		if (pList.get(idn).getX() == xval)
    			return idn;
    		if (idn > 0)
    			idn--;
    	}
    }
    
    protected static void printPointList(List<Point> plist) {
    	for (Point p : plist) {
    		System.out.print("(" + p.getX() + "," + p.getY() + ") ");
    	}
    	System.out.println("");
    }
}
