package dygraph;
import java.util.*;
import dygraph.Graph.Edge;

public class Updates {
	
	public List<Integer> nodeDels;
	public List<Edge> edgeDels;

	public List<Integer> nodeAdds;
	public List<Edge> edgeAdds;
	
	public Updates() {
		nodeDels = new ArrayList<Integer>();
		nodeAdds = new ArrayList<Integer>();
		edgeDels = new ArrayList<Edge>();
		edgeAdds = new ArrayList<Edge>();
	}
	
	public void addEdge(Integer u, Integer v) {
		edgeAdds.add(new Edge(u, v));
	}
	public void delEdge(Integer u, Integer v) {
		edgeDels.add(new Edge(u, v));
	}

	public void addNode(Integer u) {
		nodeAdds.add(u);
	}
	public void delNode(Integer u) {
		nodeDels.add(u);
	}

}
