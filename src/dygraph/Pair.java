package dygraph;

public class Pair<E> {
	
	public E L;
	public E R;
	
	public Pair() {}
	public Pair(E l, E r) {
		this.L = l;
		this.R = r;
	}
}
