package dygraph;

import java.util.HashMap;
import java.util.Map;

/*
 * Bi directional map data structure
 * implemented with two HashMaps
 */
public class BiMap<K> {

	private Map<K, K> MapL;
	private Map<K, K> MapR;

	public Map<K, K> getMapR() { return MapR; }
	public Map<K, K> getMapL() { return MapL; }
	
	public BiMap() {
		this.MapL = new HashMap<K, K>();
		this.MapR = new HashMap<K, K>();
	}
	
	public void addPair(K x, K y) {
		MapL.put(x, y);
		MapR.put(y, x);
	}
	
	public void removePair(K x, K y) {
		MapL.remove(x);
		MapL.remove(y);
		MapR.remove(x);
		MapR.remove(y);
	}
	
	public K getR(K x) {
		return MapR.get(x);
	}
	public K getL(K x) {
		return MapL.get(x);
	}

	// get y if (x, y) is present
	public K get(K x) {
		if (MapL.containsKey(x))
			return MapL.get(x);
		if (MapR.containsKey(x))
			return MapR.get(x);
		return null;
	}
	
	// change (x, y) to (xnew, y)
	public boolean changeKey(K x, K xnew) {
		K y = MapL.remove(x);
		if (y != null) {
			MapR.remove(y);
			MapL.put(xnew, y);
			MapR.put(y, xnew);
			return true;
		}
		
		return false;
	}
	
	public boolean contains(K x) {
		return (MapR.containsKey(x) || MapL.containsKey(x));
	}
}
