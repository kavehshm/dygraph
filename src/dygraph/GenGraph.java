package dygraph;
import java.util.Random;



import stream.Stream;

public class GenGraph {

	public enum genMethod {
		RANDOM
	}
	
	private static double edgeintensity = 1;
	private static Integer numnodes = 5;
	private static genMethod method = genMethod.RANDOM;
	
	public static Graph generate (double edint, Integer nnodes, genMethod mth) {
		GenGraph.edgeintensity = edint;
		GenGraph.numnodes = nnodes;
		GenGraph.method = mth;
		
		return generate();
	}
	
	public static Graph generate () {
		
		Random rand = new Random();
		
		Graph retgraph = new Graph();
		
		switch (GenGraph.method) {
		case RANDOM:
			
			for (int i = 0 ; i < numnodes; i++) {
				retgraph.addNode();
			}
			
			int numEdge = (numnodes*(numnodes-1)/2);
			numEdge = (int)(numEdge * edgeintensity);
			
			
			for (int i = 0 ; i < numEdge ; i++) {
				Integer v1, v2;
				do {
					v1 = rand.nextInt(numnodes) + 1;
					v2 = rand.nextInt(numnodes) + 1;
				}
				while (!retgraph.addEdge(v1, v2));
			}
			
			break;
		default:
			break;
		}
		
		
		return retgraph;
	}
	
	public static Graph generateDorogov (Integer numNodes) {
		org.graphstream.graph.Graph gp = Stream.genDorogov(numNodes);
		Graph dygp = new Graph(gp);
		return dygp;
	}
	
	public static Graph generate (boolean compOrsparse, Integer nnodes) {
		
		GenGraph.numnodes = nnodes;
		
		Graph retgraph = new Graph();
		
		if (compOrsparse) {
			for (int i = 0 ; i < nnodes ; i++) 
				retgraph.addNode();
			
			
			for (int i = 1 ; i <= nnodes ; i++)
				for (int j = 1 ; j <= nnodes ; j++) 
					retgraph.addEdge(i, j);

		}
		
		else {
			System.out.println("Sparse graph not implemented yet");
		}
		
		return retgraph;
	}
	
	public static Graph testGraph1 () {
		
		Graph gp = new Graph();
		
		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		
		
		gp.addEdge(1, 3);
		gp.addEdge(1, 2);
		gp.addEdge(1, 4);
		gp.addEdge(2, 3);
		gp.addEdge(4, 3);
		gp.addEdge(4, 2);
		gp.addEdge(5, 2);
		gp.addEdge(6, 2);
		gp.addEdge(5, 4);
		gp.addEdge(5, 3);
		gp.addEdge(6, 1);
		gp.addEdge(6, 4);
		gp.addEdge(7, 2);
		gp.addEdge(7, 1);
		gp.addEdge(8, 4);
		gp.addEdge(8, 5);
		
		
		return gp;
	}
	
	public static Graph testGraph2 () {
		
		Graph gp = new Graph();
		
		gp.createNodes(23);
		
		gp.addEdge(1, 2);
		gp.addEdge(2, 11);
		gp.addEdge(2, 12);
		gp.addEdge(1, 3);
		gp.addEdge(3, 4);
		gp.addEdge(4, 5);
		gp.addEdge(4, 6);
		gp.addEdge(5, 7);
		gp.addEdge(7, 8);
		gp.addEdge(8, 9);
		gp.addEdge(8, 10);
		
		gp.addEdge(6, 13);
		gp.addEdge(6, 14);
		gp.addEdge(14, 20);
		gp.addEdge(14, 21);
		gp.addEdge(20, 22);
		gp.addEdge(22, 23);
		//gp.addEdge(6, 15);
		
		gp.addEdge(9, 16);
		gp.addEdge(9, 17);
		gp.addEdge(10, 18);
		gp.addEdge(10, 19);
		
		
		
		return gp;
	}
	
	public static Graph testGraph3 () {
		
		Graph gp = new Graph();
		
		gp.createNodes(9);
		
		gp.addEdge(1, 2);
		gp.addEdge(2, 9);
		gp.addEdge(9, 6);
		gp.addEdge(1, 3);
		gp.addEdge(3, 4);
		gp.addEdge(2, 5);
		gp.addEdge(6, 7);
		gp.addEdge(1, 7);
		gp.addEdge(6, 8);
		gp.addEdge(8, 9);
		gp.addEdge(1, 9);
		gp.addEdge(2, 7);
		
		return gp;
	}
	
	public static Graph testGraph4 () {
		
		Graph gp = new Graph();
		
		gp.createNodes(9);
		
		gp.addEdge(1, 2);
		gp.addEdge(2, 9);
		gp.addEdge(9, 6);
		gp.addEdge(1, 3);
		gp.addEdge(3, 4);
		gp.addEdge(2, 5);
		gp.addEdge(6, 2);
		gp.addEdge(8, 9);
		gp.addEdge(1, 9);
		gp.addEdge(7, 2);
		
		return gp;
	}

	
	public static Graph testGraph5 () {
		
		Graph gp = new Graph();
		
		gp.createNodes(4);
		
		gp.addEdge(1, 2);
		gp.addEdge(1, 3);
		gp.addEdge(2, 3);
		gp.addEdge(3, 4);
		gp.addEdge(4, 2);

		
		return gp;
	}

	
	public static Graph testGraph6 () {
		
		Graph gp = new Graph();
		
		gp.createNodes(6);
		
		gp.addEdge(1, 2);
		gp.addEdge(1, 5);
		gp.addEdge(1, 6);
		gp.addEdge(1, 3);
		gp.addEdge(1, 4);
		gp.addEdge(2, 4);
		gp.addEdge(2, 3);
		gp.addEdge(4, 5);
		gp.addEdge(5, 6);

		
		return gp;
	}
	
	public static Graph testGraph7 () {
		
		Graph gp = new Graph();
		
		gp.createNodes(9);
		
		gp.addEdge(1, 2);
		gp.addEdge(2, 9);
		gp.addEdge(9, 6);
		gp.addEdge(1, 3);
		gp.addEdge(3, 4);
		gp.addEdge(2, 5);
		gp.addEdge(6, 8);
		gp.addEdge(8, 9);
		gp.addEdge(1, 9);
		gp.addEdge(7, 6);
		gp.addEdge(7, 2);
		
		return gp;
	}

	
	public static void main (String args[]) {
		/*
		Graph gp = GenGraph.testGraph2();
		stream.Stream dystr = new stream.Stream(gp);
		dystr.plotGraph();
		*/
		
		Graph gp = GenGraph.generateDorogov(2);
		gp.addNode();
	}
	
}
