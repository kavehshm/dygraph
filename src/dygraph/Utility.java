package dygraph;

import java.util.*;

public class Utility {
	
	public static void main(String args[]){
		System.out.println("random list");
		System.out.println(randomList(15, 65, 10));
	}
	
	static <E> void pushAlltoStack(Stack<E> stack, List<E> list) {
		
		for (E elem: list) {
			stack.push(elem);
		}
		return;
	}
	
	static List<Integer> randomList(int low, int high, int size) {
		
		List<Integer> ret = new ArrayList<Integer>();
		Set<Integer> selected = new HashSet<Integer>();
		Random rng = new Random();
		int n = (high - low + 1);
		
		while (ret.size() < size) {
			Integer randnum = rng.nextInt(n) + low; 
			if (!selected.contains(randnum)) {
				selected.add(randnum);
				ret.add(randnum);
			}
		}
		
		return ret;
		
	}
	
	public static <E> E lastItem(List<E> lst) {
		return lst.get(lst.size()-1);
	}
	public static <E> E firstItem(List<E> lst) {
		return lst.get(0);
	}
	
	static ArrayList<Integer> consecList(int low, int high) {
		ArrayList<Integer> retList = new ArrayList<Integer>();
		for (int i = low; i <= high; i++) {
			retList.add(i);
		}
		return retList;
	}

}
