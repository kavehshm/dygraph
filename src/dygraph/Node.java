package dygraph;

public class Node {
	
	// primary attribute
	private Integer id;
	
	
	// methods
	public Node() {
		id = 0;
	}
	
	public Node(Integer id) {
		this.id = id;
	}
	
	public Integer id() { return id; }
	
}
