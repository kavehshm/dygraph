#!/bin/bash
/usr/bin/java -Dfile.encoding=UTF-8 -classpath ./bin/:./javaclass/gs-ui-1.3.jar:./javaclass/gs-core-1.3.jar:./javaclass/gs-algo-1.3.jar dygraph.DfsTree

/usr/bin/java -Dfile.encoding=UTF-8 -classpath ./bin:./javaclass/gs-ui-1.3.jar:./javaclass/gs-core-1.3.jar:./javaclass/gs-algo-1.3.jar dygraph.DisjPartition

/usr/bin/java -Dfile.encoding=UTF-8 -classpath ./bin:./javaclass/gs-ui-1.3.jar:./javaclass/gs-core-1.3.jar:./javaclass/gs-algo-1.3.jar dygraph.BasDstruct

/usr/bin/java -Dfile.encoding=UTF-8 -classpath ./bin:./javaclass/gs-ui-1.3.jar:./javaclass/gs-core-1.3.jar:./javaclass/gs-algo-1.3.jar dygraph.DyDfsTree
