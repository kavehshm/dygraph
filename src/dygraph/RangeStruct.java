package dygraph;

import dygraph.BinTree.BSTree;

public class RangeStruct {
	
	private class RootYtree extends BSTree <Byte> {};
	public RootYtree rootytree;
	
	
	// fractional-cascading ylist element
	public class FracCasNode {
		public Integer ycor;
		public Integer rpointer;
		public Integer lpointer;
		
		public FracCasNode(Integer ycor, Integer rpointer, Integer lpointer) {
			this.ycor = ycor;
			this.rpointer = rpointer;
			this.lpointer = lpointer;
		}
	}
	
	

}
