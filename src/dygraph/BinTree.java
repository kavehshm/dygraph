package dygraph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/*
 *  general class for binary trees
 */
public class BinTree {
	
	/*
	 * top class for binary search trees with integer key values
	 */
	public static class BSTree <Data> {
		
		static boolean verbose = false;
		
		//main for testing
		public static void main(String args[]) {
			if (verbose) System.out.println("verbose mode...\n\n");
			
			//ArrayList<Integer> testlist = (ArrayList<Integer>) Utility.randomList(5, 30, 10);
			ArrayList<Integer> testlist = 
					new ArrayList<Integer>( Arrays.asList(14,17,21,23,24) );
			Collections.sort(testlist);
			
			System.out.println(testlist);
			
			BSTree<Integer> myrtree = new BSTree<Integer> (testlist);
			
			//myrtree.query1Drange(17, 24);
			//System.out.println((myrtree.successor(24) == null) ? "was null" : "");
			assert myrtree.predecessor(14, 1).key == 14;
			assert myrtree.predecessor(16, 1).key == 14;
			assert myrtree.predecessor(21, 1).key == 21;
			assert myrtree.predecessor(22, 1).key == 21;
			assert myrtree.predecessor(19, 18, 1) == null;
			assert myrtree.predecessor(25, 1).key == 24;
			assert myrtree.predecessor(13, 1) == null;
			
			assert myrtree.successor(14, 1).key == 14;
			assert myrtree.successor(14, 0).key == 17;
			assert myrtree.successor(16, 0).key == 17;
			assert myrtree.successor(16, 1).key == 17;
			assert myrtree.successor(24, 1).key == 24;
			assert myrtree.successor(24, 0) == null;
			assert myrtree.successor(18, 19, 0) == null;
			assert myrtree.successor(13, 0).key == 14;
			
			System.out.println("passed all tests");
			
		
			//myrtree.insert(15);
			//if (!myrtree.delete(30))
			//	System.out.println("\nnode to be deleted not found\n");
			//myrtree.printTree();
			
		}	

		
		/*
		 * data
		 */
		protected Node root;
		
		/*
		 * Top Node classe
		 */
		
		
		protected class Node {
			
			public Integer key;
			public Data data;
			
			public Node left, right;

			public Node() {
				this.key = 0;
				this.data = null;
			}
			public Node(Integer key) {
				this.key = key;
				this.data = null;
			}
			
			public Node(Integer key, Data data) {
				this.key = key;
				this.data = data;
			}
			
			boolean IsLeaf() {
				return (left == null) && (right == null);
			}
			
			public Node childThatIs(Node node) { 
				return (left == node) ? left : right;
			}
			public Node childThatIsNot(Node node) { 
				return (left == node) ? right : left;
			}
		}

	
		/*
		 *  Methods
		 */
		public BSTree() { this.root = new Node(); }
		public Node getroot() { return root; }
		
		// assumes the XList is sorted
		public BSTree (ArrayList<Integer> XList) {
			this.root = new Node();
			buildTree(root, XList);
			//printTree(root, 0);
			//System.out.println("\n");
		}
		
		protected void buildTree(Node node, List<Integer> XList) {
			if (XList.size() == 0)
				return;

			if (XList.size() == 1) {
				node.key = XList.get(0);
				return;
			}
			
			else {
				Integer xmed = ((XList.size()-1)/2);
				node.key = XList.get(xmed);
				node.left = new Node();
				node.right = new Node();
				buildTree(node.left, XList.subList(0, xmed+1));
				buildTree(node.right, XList.subList(xmed+1, XList.size()));
			}
		}
		
		
		
		/*
		 * update methods
		 */
		public void insert(Integer x) {
			insert(root, x);	
		}
		private void insert(Node node, Integer x) {
			if (node.key == x) {
				System.out.println("duplicate insertion");
				return;
			}
			if (node.IsLeaf()) {
				if (x < node.key) {
					node.left = new Node(x);
					node.right = new Node(node.key);
					node.key = x;
				}
				else {
					node.right = new Node(x);
					node.left = new Node(node.key);
				}
				return;
			}
			if (x < node.key) 
				insert(node.left, x);
			else
				insert(node.right, x);
		}
		
		public boolean delete(Integer x) {
			return delete(root, null, null, x);
		}
		private boolean delete(Node node, Node par, Node parpar, Integer x) {
			if (node.IsLeaf()) {
				if (node.key != x)
					return false;
				else {
					removeLeafandPar(node, par, parpar);
					return true;
				}
			}
			else 
				return (x > node.key)? 
					delete(node.right, node, par, x) : delete(node.left, node, par, x);
		}
		
		public void delete(Integer x, Integer d) {
			delete(root, null, null, x, d);
		}
		private void delete(Node node, Node par, Node parpar, Integer x, Integer d) {
			if (node.IsLeaf()) {
				if ((node.key != x) || (node.data != d))
					return;
				else {
					removeLeafandPar(node, par, parpar);
					return;
				}
			}
			else {
				if (x > node.key) {
					delete(node.right, node, par, x);
				}
				else {
					if (x.equals(node.right.key))
						delete(node.right, node, par, x);
					delete(node.left, node, par, x);
				}
			}
		}
		
		protected void removeLeafandPar(Node node, Node par, Node parpar){
			if (parpar == null) {
				if (par == null) {
					node.data = null;
					node.key = -1;
					return;
				}
				root = par.childThatIsNot(node);
				return;
			}
			if (parpar.left == par)
				parpar.left = par.childThatIsNot(node);
			else
				parpar.right = par.childThatIsNot(node);
		
		}
		
		
		/*
		 * Search methods
		 */
		public Node minimum(Node node) {
			if (node.IsLeaf()) 
				return node;
			else
				return minimum(node.left);
		}

		public Node maximum(Node node) {
			if (node.IsLeaf()) 
				return node;
			else
				return maximum(node.right);
		}
		
		public Node findKey(Integer k) {
			return findKey(root, k);
		}
		public Node findKey(Node node, Integer k) {
			if ((node.key == k) || node.IsLeaf())
				return node;
			
			if (k > node.key)
				return findKey(node.right, k);
			else
				return findKey(node.left, k);
		}
		
		// successor and predecessor queries
		public Node successor(Integer k, int inclusive) {
			Node nd = findKey(k);
			if (k > nd.key)
				return null;
			if (!nd.IsLeaf()){ 
				if (inclusive == 1)
					return maximum(nd.left);
				else
					return minimum(nd.right);
					
			}
			
			if (k == nd.key && inclusive == 0)
				return null;
			return nd;
		}
		
		public Node successor(Integer k, Integer max, int inclusive) {
			Node retnd = successor(k, inclusive);
			if (retnd == null)
				return null;
			if (retnd.key > max) 
				return null;
			else
				return retnd;
		}

		// successor and predecessor queries
		public Node predecessor(Integer k, int inclusive) {
			
			Stack<Node> ndstack = new Stack<Node>();
			
			ndstack.push(root);
			Node curnode = new Node();
			curnode = root;
			while (true) {
				if (curnode.IsLeaf()) {
					if (curnode.key <= k)
						return curnode;
					do {
						Node backtrace = ndstack.pop(); 
						if (backtrace.key < k) 
							return maximum(backtrace.left);
					} while (!ndstack.isEmpty());
					return null;
				}
				
				if (curnode.key == k) 
					return maximum(curnode.left);
				
				if (k > curnode.key) {
					curnode = curnode.right;
				}
				else {
					curnode = curnode.left;
				}
				ndstack.push(curnode);
				
			}
			
		}
		
		
		public Node predecessor(Integer k, Integer min, int inclusive) {	
			Node retnd = predecessor(k, inclusive);
			if (retnd == null)
				return null;
			if (retnd.key < min)
				return null;
			else
				return retnd;
			
		}
		
		
				
		
		/*
		 * Query methods
		 */
		public void query1Drange(Integer x1, Integer x2) {
			Report<Data> r = new Range1DReport<Data>();
			queryRange(x1, x2, r);
		}
		public void queryRange(Integer x1, Integer x2, Report<Data> r) {
			if (x1 == x2) {
				r.reportTree(findKey(x1));
				return;
			}
			
			Node vsplit = findSplitNode(x1, x2);
			if (vsplit == null) {
				if (verbose) System.out.println("vsplit not found");
				return;
			}
			if (vsplit.IsLeaf()) {
				r.reportTree(vsplit);
				return;
			}
			if (verbose) System.out.println("vsplit: " + vsplit.key);
			
			queryR(vsplit.right, x2, r);
			queryL(vsplit.left, x1, r);
		}
		
		public Node findSplitNode(Integer x1, Integer x2) {
			
			Node curnode = root;
			while (true) {
				if ( ((x1 > curnode.key) && (x2 <= curnode.key)) ||
					((x1 <= curnode.key) && (x2 > curnode.key)) )
					return curnode;
	
				else if (curnode.IsLeaf())
					return null;
				else if (x1 > curnode.key) 
					curnode  = curnode.right;
				else 
					curnode = curnode.left;
			}
		}
		
		public void queryR(Node node, Integer x2, Report<Data> r) {
			
			if (node.IsLeaf()) {
				if (x2 >= node.key)
					r.reportTree(node);
				return;
			}
			
			if (x2 >= node.key) {
				r.reportTree(node.left);
				queryR(node.right, x2, r);
			}
			else {
				queryR(node.left, x2, r);
			}
		}
		
		public void queryL(Node node, Integer x1, Report<Data> r) {
			
			if (node.IsLeaf()) {
				if (x1 <= node.key)
					r.reportTree(node);
				return;
			}
			
			if (x1 <= node.key) {
				r.reportTree(node.right);
				queryL(node.left, x1, r);
			}
			else {
				queryL(node.right, x1, r);
			}
		}
		
		/*
		 * Reporter interface and class
		 */
		public static interface Report<Data> {
			public void reportTree(BinTree.BSTree<Data>.Node node);
		}
		
		public static class Range1DReport<Data> implements Report<Data> {
			public void reportTree(BinTree.BSTree<Data>.Node node) {
				if (node.IsLeaf()) {
					System.out.println("node found: " + node.key);
					return;
				}
				reportTree(node.left);
				reportTree(node.right);
			}
		}

		
		/*
		 * print or report methods
		 */
		public void printTree() {
			System.out.println("");
			printTree(root, 0);
			System.out.println("\n");
		}
		public void printTree(Node node, int level){
		    if(node == null)
		         return;
		    printTree(node.right, level+1);
		    if(level != 0){
		        for(int i = 0; i < level-1; i++)
		            System.out.print("|\t");
		        System.out.println("|-------"+node.key);
		    }
		    else
		        System.out.println(node.key);
		    printTree(node.left, level+1);
		}
		
	}
	
	
	
}