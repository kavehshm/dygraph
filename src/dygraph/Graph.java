package dygraph;

import java.lang.System;
import java.util.*;

/**
 * 
 * @author kaveh
 * a class for representing graphs and dynamic DFS
 */
public class Graph {
    
	// necessary metadata
	private Integer numNodes;
	private Integer numEdges;
	private final Integer r = 0;
	
	// adjacency list
	private Map< Integer, ArrayList<Integer> > adj;
	
	// constructor
	public Graph(){
		adj = new HashMap<Integer, ArrayList<Integer> >();
		numNodes = 0;
		numEdges = 0;
	}
	
	//getter setter methods
	public Integer getnumNodes() { return numNodes; }
	public Integer getnumEdges() { return numEdges; }
	public Map<Integer, ArrayList<Integer> > getadj() {return adj; }

	
	public ArrayList<Integer> getNeighbors(Integer nd) {
		return adj.get(nd);
	}
	
	public Integer getRnode() { return this.r; }
	
	public ArrayList<Integer> getNodeList() {
		ArrayList<Integer> nodelist = new ArrayList<Integer>(adj.keySet());
		return nodelist;
	}
	
	public Set<Integer> getNodeSet() {
		return adj.keySet();
	}
	
	public ArrayList<Edge> getEdgeList(Integer nd) {
		ArrayList<Edge> edgelist = new ArrayList<Edge>();
		for (Integer i : adj.get(nd)) {
			if ((i != 0) && (nd != 0))
				edgelist.add(new Edge(nd, i));
		}
		return edgelist;
	}
	
	public ArrayList<Edge> getEdgeListwR(Integer nd) {
		ArrayList<Edge> edgelist = new ArrayList<Edge>();
		for (Integer i : adj.get(nd)) {
				edgelist.add(new Edge(nd, i));
		}
		return edgelist;
	}
	
	public Set<Edge> getAllEdgesSet() {
		Set<Edge> retSet = new HashSet<Edge>();
		for (Integer nd : getNodeList()) {
			ArrayList<Edge> edgelist = getEdgeList(nd);
			retSet.addAll(edgelist);
		}
		return retSet;		
	}
	
	public Set<Edge> getAllEdgesSetwR() {
		Set<Edge> retSet = new HashSet<Edge>();
		for (Integer nd : getNodeList()) {
			ArrayList<Edge> edgelist = getEdgeListwR(nd);
			retSet.addAll(edgelist);
		}
		return retSet;		
	}
	
	// add node with id = number of nodes
	// node 0 is reserved
	public Integer addNode() {
		ArrayList<Integer> nodelist = new ArrayList<Integer>();
		adj.put(++numNodes, nodelist);
		return numNodes;
	}
	
	public void createNodes(int numNodes) {
		for (int i = 0; i < numNodes; i++) {
			addNode();
		}
	}
	
	public Integer addNode(Integer id) {
		ArrayList<Integer> nodelist = new ArrayList<Integer>();
		adj.put(id, nodelist);
		return numNodes++;
	}
	
	public DfsTree staticDFS() {
		DfsTree retTree = new DfsTree(this);
		
		long startTime = System.nanoTime();
		retTree.staticDFS();
		System.out.println("static DFS took: " 
				+ (float) (System.nanoTime() - startTime)/1000);
		
		retTree.buildL();
		retTree.buildLastMap();
		//retTree.print();
		return retTree;
	}
	
	public boolean addEdge(Integer v1, Integer v2) {
		int fail = -2;
		if (!adj.containsKey(v1)) 
			fail = v1;
		if (!adj.containsKey(v2)) 
			fail = v2;
		
		if (fail != -2) {
			System.err.println("node " + fail + " not found during addEdge function");
			System.exit(1);
		}
	
		
		if (v1 == v2) 
			return false;
		
		if ( !adj.get(v1).contains(v2) )
			adj.get(v1).add(v2);
		if ( !adj.get(v2).contains(v1) )
			adj.get(v2).add(v1);
		
		numEdges++;
		return true;
	}
	
	public void print() {
		for (Map.Entry<Integer, ArrayList<Integer>> node : adj.entrySet()) {
			System.out.print(node.getKey() + " : ");
			List<Integer> w = node.getValue();
			for (int i = 0 ;  i < w.size(); i++) {
				System.out.print(w.get(i));
				if (i != w.size()-1) 
					System.out.print(" -> ");
			}
			System.out.print("\n");
		}
	}
	
	
	/*
	 * The Edge class
	 */
	public static class Edge extends Pair<Integer> {
		public Edge (Integer u, Integer v) {
			super(new Integer(u),new Integer(v));
		}
		
		public Integer getU() { return L; }
		public Integer getV() { return R; }
		
		@Override
		public boolean equals(Object o) {
			if (o instanceof Edge) {
				Edge e = (Edge) o;
				return (this.getU().equals(e.getU()) && this.getV().equals(e.getV()) ) ||
						(this.getU().equals(e.getV()) && this.getV().equals(e.getU()) );
			}
			else {
				return false;
			}
		}
		@Override
		public int hashCode() {
			return L.hashCode() ^ R.hashCode();
		}
	    
		@Override
	    public String toString() {
	        return String.format("(" + R + ", " + L + ")");
	    }
		
		public void printEdge() {
			System.out.print("(" + L + "," + R + ") ");
		}
		public static void printEdgeList(List<Edge> edgelist) {
			for (Edge e : edgelist) {
				e.printEdge();
			}
			System.out.println();
		}
	};
	
	/*
	 * Converting graphstream graph to dygraph.Graph
	 */
	public Graph(org.graphstream.graph.Graph graph) {
		adj = new HashMap<Integer, ArrayList<Integer> >();
		numNodes = 0;
		numEdges = 0;
		
		createNodes(graph.getNodeCount());
		
		for (org.graphstream.graph.Edge e : graph.getEachEdge()) {
			this.addEdge(e.getNode0().getIndex()+1, e.getNode1().getIndex()+1);
		}
		
		print();
	}
	
	
	public static void main(String args[]){
		
		Graph gp = new Graph();

		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		gp.addNode();
		
		gp.addEdge(1, 3);
		gp.addEdge(1, 2);
		gp.addEdge(1, 4);
		gp.addEdge(2, 3);
		gp.addEdge(4, 3);
		gp.addEdge(4, 2);
		
		System.out.println(gp.getAllEdgesSet());
		System.out.println("using integers as nodes");
		gp.print();
    }  	
}

