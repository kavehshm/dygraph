package dygraph;

import java.util.ArrayList;
import java.util.Map;


public abstract class Dstruct {
	
	protected DfsTree T;
	protected Graph gp;
	
	protected ArrayList<Integer> Lst;
	protected Map<Integer, Integer> LstMap;
	protected Map<Integer, Integer> LastLocMap;

	public Dstruct(Graph gp, DfsTree dfstree) {
		this.Lst = dfstree.Lst;
		this.LstMap = dfstree.LstMap;
		this.T = dfstree;
		this.gp = gp;
		
		this.LastLocMap = dfstree.LastNodeMap;
	}	
}
